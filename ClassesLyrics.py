# -*- coding: utf-8 -*-
"""
Created on Wed Jan 12 10:52:11 2022

@author: Admin
"""




#get_ipython().system(' pip install nltk')

import pandas as pd
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import nltk
from nltk.corpus import stopwords
from nltk import bigrams
import itertools, collections
# Download stopwords
nltk.download('stopwords')

class collocation:
    def __init__(self, serie_chanson):
        self.paroles = serie_chanson['Paroles']
        self.artiste = serie_chanson['Artistes']
        self.album = serie_chanson['Albums']
        self.titre = serie_chanson['Titres']
        self.date = serie_chanson['Dates']
        
    def __srt__(self):
        #return("En : ", self.date, ", l'artiste : ", self.artiste, " sort le titre : ", self.titre)
        return f"En : {self.date}, l'artiste : {self.artiste} sort le titre {self.titre}"    
    
    
    def nbr_caractere(self): # la longueur du corpus
    # On concatène les paroles dans un même corpus
        corpus = ''
        for parole in self.paroles :
            corpus = corpus + parole
        return len(corpus)

    def nbr_phrase(self): # le nombre de phrase du corpus
        # On concatène les paroles dans un même corpus
        corpus = ''
        for parole in self.paroles :
            corpus = corpus + parole
        return len(corpus.split("\r"))

    def nbr_mot(self): # le nombre de mot du corpus
        # On concatène les paroles dans un même corpus
        corpus = ''
        for parole in self.paroles :
            corpus = corpus + parole
        return len(corpus.split(" "))

    def moy_nbr_phrase(self): # la moyenne du nombre de phrases dans un corpus
        nb_phrases = [len(doc.split("\r")) for doc in self.paroles]
        return np.mean(nb_phrases)

    def moy_nbr_mot(self): # la moyenne du nombre de mots dans un corpus 
        nb_mots = [len(doc.split(" ")) for doc in self.paroles]
        return np.mean(nb_mots)
    
    def statistiques(self): # rassemble les fonctions au dessus
        print("Longueur du corpus (caractères) : " + str(self.nbr_caractere()))

        print("Nombre total de phrases : " + str(self.nbr_phrase()))

        print("Nombre total de mots : " + str(self.nbr_mot()))
        
        print("Moyenne du nombre de phrases dans une chanson : " + str( self.moy_nbr_phrase()))
        
        print("Moyenne du nombre de mots dans une chanson : " + str(self.moy_nbr_mot()))
        
    def suppr_ponctuation(self, txt): # la suppression des ponctuations et des retours à la ligne
        txt = txt.replace("\n", " ")
        txt = txt.replace("\r", " ")
        txt = txt.replace("  ", " ")
        txt = txt.replace("'", " ")
        ponctuations = ",;():.-!?\""
        for i in txt:
            if i in ponctuations:
                txt = txt.replace(i,"")
        return txt

    def cpt_frequence(self, txt, freq): # liste des mots les plus fréquents dans le corpus
        mot_cpt = {}
        tab = txt.split(" ")

        # Suppression des mots usuels
        stopWords = set(stopwords.words('french'))
        stopWords.update(['ooh','oh','eh','là','i','faut','vais','woh','veut','p','v','depuis','veux','oui','dit','you','nan',
                          'ah','bam','fait','ça','wa','ah','comme','ouais','alors','a','mi','tout','por','si',
                          'ici','fais','hey','autre','are','now','it','tellement','quand',
                          'sais','faire','être','va','plus','sans','trop','tous'])
        mots_nsw = [mot for mot in tab if not mot in stopWords]

        for mot in mots_nsw :
            if mot in mot_cpt:
                mot_cpt[mot] += 1
            else :
                mot_cpt[mot] = 1

         # Trier par ordre décroissant       
        for i in sorted(mot_cpt, key=mot_cpt.get, reverse=True):
            if (len(i)>=1 and mot_cpt[i]>=freq):
                print(i,' : ', mot_cpt[i])
        return mot_cpt

    def frequence(self, freq): # rassemble les fonctions nécessaires à l'analyse de fréquence
        liste  = []
        for parole in self.paroles:
            parole_no_ponct = self.suppr_ponctuation(parole)  # Suppression des ponctuations
            parole_no_ponct_lc = parole_no_ponct.lower() # Mettre en minuscule
            liste.append(parole_no_ponct_lc)
        liste_mot = pd.Series(liste)
        self.cpt_frequence(liste_mot.str.cat(sep=' '), freq)
        
    def get_bigram(self): # graphe bigrames des mots les plus fréquents
        # Suppression des ponctuations
        liste = []
        for parole in self.paroles:
            parole_no_ponct = self.suppr_ponctuation(parole)  
            liste.append(parole_no_ponct)
            liste_mot = pd.Series(liste)

        # Create a sublist of lower case words for each song
        paroles_lw = [son.lower().split() for son in liste_mot]

        # Remove stop words from each song list of words
        stopWords = set(stopwords.words('french'))
        stopWords.update(['ooh','oh','eh','là','i','faut','vais','woh','veut','p','v','depuis','veux','oui','dit','you','nan',
                          'ah','bam','fait','ça','wa','ah','comme','ouais','alors','a','mi','tout','por','si',
                          'ici','fais','hey','autre','are','now','it','tellement','quand',
                          'sais','faire','être','va','plus','sans','trop','tous'])
        mots_nsw = [[mot for mot in mots if not mot in stopWords]
                  for mots in paroles_lw]

        # Create list of lists containing bigrams in tweets
        terms_bigram = [list(bigrams(mot)) for mot in mots_nsw]

        # Flatten list of bigrams in clean tweets
        bgr = list(itertools.chain(*terms_bigram))

        # Create counter of words in clean bigrams
        bigram_counts = collections.Counter(bgr)

    #     print(bigram_counts.most_common(20))
        return bigram_counts
    
    def print_bigram(self, freq): # visualisation du graphe bigrames
        counts = self.get_bigram()
        bigram_df = pd.DataFrame(counts.most_common(freq),
                                 columns=['bigram', 'count'])

        # Create dictionary of bigrams and their counts
        d = bigram_df.set_index('bigram').T.to_dict('records')

        # Create network plot 
        G = nx.Graph() 

        # Create connections between nodes
        for k, v in d[0].items():
            G.add_edge(k[0], k[1], weight=(v * 10))

        fig, ax = plt.subplots(figsize=(10, 8))

        pos = nx.spring_layout(G, k=2)

        # Plot networks
        nx.draw_networkx(G, pos,
                         font_size=16,
                         width=3,
                         edge_color='grey',
                         node_color='purple',
                         with_labels = False,
                         ax=ax)

        # Create offset labels
        for key, value in pos.items():
            x, y = value[0]+.135, value[1]+.045
            ax.text(x, y,
                    s=key,
                    bbox=dict(facecolor='red', alpha=0.25),
                    horizontalalignment='center', fontsize=13)

        plt.show()
